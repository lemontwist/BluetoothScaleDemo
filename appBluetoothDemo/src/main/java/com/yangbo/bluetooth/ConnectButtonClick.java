package com.yangbo.bluetooth;

/* ListView 内部按钮点击事件接口 */
public interface ConnectButtonClick {
    void connectBtnClick(int position);
}
